resource "aws_instance" "app_server" {
  ami           = "ami-0b0af3577fe5e3532"
  instance_type = "t2.micro"

  tags = {
    Name        = var.instance_name
    Terraform   = "true"
    Environment = "dev"
  }
}
