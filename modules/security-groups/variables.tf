variable "vpc_id" {
  default = "vpc-7743e90a"
}
variable "ingress_cidr_blocks" {
  description = "cidr block or Prefix list"
  type        = string
}
