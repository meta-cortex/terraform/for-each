resource "aws_security_group" "web_servers" {
  name        = "web_servers"
  description = "Allow web server inbound traffic"
  vpc_id      = data.aws_vpc.selected.id

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    prefix_list_ids = [var.ingress_cidr_blocks]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "web_servers"
    Terraform   = "true"
    Environment = "dev"
  }
}
