terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

module "subnet_prefix_list" {
  source = "./modules/prefix-list"
}


module "example_aws_instance" {
  source = "./modules/aws-instance"
}


module "web_server_security_groups" {
  source              = "./modules/security-groups"
  ingress_cidr_blocks = data.aws_prefix_list.subnet_prefix_list.cidr_blocks[0]
  # ingress_cidr_blocks = module.subnet_prefix_list.id
}

data "aws_prefix_list" "subnet_prefix_list" {
  prefix_list_id = module.subnet_prefix_list.webserver_prefix_list_id
}
